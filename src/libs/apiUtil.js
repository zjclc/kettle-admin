import config from '@/config/config';

var apiUtil = {};
apiUtil.getApiBaseUrl = function () {
  return process.env.NODE_ENV === 'development' ? config.apiBaseUrl.dev : config.apiBaseUrl.pro;
};
apiUtil.baseUrl = function () {
  return process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro;
};


export default apiUtil;
